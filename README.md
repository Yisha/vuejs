## Vue.js - Rails Example

The purpose of this project is to demonstrate how to incorporate [Vue.js](http://vuejs.org) into a Rails application.  Checkout the demo and tutorial:

[Demo](https://blooming-everglades-98702.herokuapp.com)

[Tutorial]
(https://rlafranchi.github.io/2016/03/09/vuejs-and-rails)
This is not my repo, all rights from https://tutorialzine.com/2016/03/5-practical-examples-for-learning-vue-js